//
//  LaureatesTableViewController.swift
//  SuperheroesandLaureates
//
//  Created by Student on 4/13/19.
//  Copyright © 2019 Gandra, Akhila. All rights reserved.
//

import UIKit

class LaureatesTableViewController: UITableViewController {
    let laureatesdata = "https://www.dropbox.com/s/7dhdrygnd4khgj2/laureates.json?dl=1"
    var laureates:[Laureates] = []
    func displayLaureatessInTableView(data:Data?, urlResponse:URLResponse?, error:Error?)->Void {
        
        var laureatemems:[[String:Any]]!
        var laureats:[String:Any]!
        do {
            try laureatemems = JSONSerialization.jsonObject(with: data!, options: .allowFragments)  as?  [[String:Any]]
            if laureatemems != nil {
                
                for i in 0 ..< laureatemems.count{
                    laureats = laureatemems[i]
                    let firstname = laureats["firstname"] as? String
                    let surname = laureats["surname"] as? String
                    let born = laureats["born"] as? String
                    let died = laureats["died"] as? String
                    laureates.append(Laureates(firstname: firstname, surname: surname, born: born, died: died))
                }
                for onelaureate in laureats{
                    print(onelaureate)
                }
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    NotificationCenter.default.post(name: NSNotification.Name("laureates coming"), object: self.laureates)
                }
            }
        } catch {
            print(error)
        }
        
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let urlSession = URLSession.shared
        let url = URL(string: laureatesdata)
        urlSession.dataTask(with: url!, completionHandler: displayLaureatessInTableView).resume()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return laureates.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Laureates", for: indexPath)
        let nameLBL = cell.viewWithTag(175) as! UILabel
        let dateLBL = cell.viewWithTag(290) as! UILabel
        nameLBL.text = "\(laureates[indexPath.row].firstname ?? "") \( laureates[indexPath.row].surname ?? "") "
        dateLBL.text = "\(laureates[indexPath.row].born ?? "") \(laureates[indexPath.row].died ?? "")"
        return cell
        
    }
    /*
     override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
     
     // Configure the cell...
     
     return cell
     }
     */
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
